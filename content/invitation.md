---
title: Lettre d’invitation
description: "Nous pouvons fournir aux participat.e.s une lettre d’invitation pour l’événement."
---

Nous pouvons fournir aux participat.e.s une lettre d’invitation pour l’événement.
Ce document précise les dates du colloque, le lieu et le titre de votre intervention.
Utilisez le formulaire ci-dessous pour nous adresser votre demande.
Merci d’indiquer votre adresse institutionnelle et, le cas échéant, toute précision à apporter à la lettre.