---
title: Activités sociales
description: "Diverses activités – exposition, lancements de livres, spectacles – sont au programme du colloque."
---

Durant la conférence, une table de consultation sera mise à la disposition des participants pour y présenter leurs publications.
Une table de vente sera également disponible.

*Activités à faire à Montréal: à préciser*

.

.

.

.

.

.

.

.

.

.

.

.
