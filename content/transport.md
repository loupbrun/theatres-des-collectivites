---
title: Transport
description: "Montréal est très bien desservie en moyens de transport en commun.  Le métro est très efficace."
---

## À partir de l’aéroport

- **Taxi** : Le coût est d’environ 40,00$ CAD de l’aéroport P.-E. Trudeau au centre-ville de Montréal.
- **Autobus** : L’autobus 747 fait la navette entre l’aéroport P.-E. Trudeau et la station de métro Lionel-Groulx.
  De là, il est possible de se rendre à l’Université par le métro.
  Les titres de transport peuvent être achetés à l’aéroport (10,00$ CAD). 

## Dans Montréal 

Il est possible de se déplacer dans tout Montréal à l’aide du transport en commun, soit l’autobus ou le métro.
Il existe différents types de titres de transport pouvant accommoder vos besoins.
Pour plus d’informations, consultez le site de la STM :
http://www.stm.info/fr
