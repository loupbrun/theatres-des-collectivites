---
title: Comité organisateur
description: "Voici les membres du comité organisateur pour l’édition 2020 du colloque Théâtres des collectivités."
---

- **Jean-Marc Larrue** (Université de Montréal)
- **Chloée Ferland-Dufresne** (Université de Montréal)
- **Dominic Poulin** (Université de Montréal)
- **Yoland Roy** (FQTA)

Logistique:

- Information scientifique : Philippe Manevy
- Informations générales : Karolann St-Amand
- Hébergement et transport : Camille Gascon
- Support technique : Louis-Olivier Brassard

