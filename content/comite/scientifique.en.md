---
title: Scientific Committee
---

## In Charge, Scientific Section 

- **Jean-Marc Larrue** (University of Montréal)
- **Philippe Manevy** (Université of Montréal)

## Scientific Committee

- **Alain Chevalier** (Université de Liège, Belgium)
- **Elka Fedjuk** (Universidad Veracruzana, Mexico)
- **Kathryn Mederos Syssoyeva** (Dixie State University, USA)
- **Françoise Odin** (INSA-Lyon, France)
- **Robin Whittaker** (St. Thomas University, Canada)
- **Vito Minoia** (AENIGMA, University of Urbino, Italy)

