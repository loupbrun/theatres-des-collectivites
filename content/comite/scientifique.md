---
title: Comité scientifique
---

## Responsables scientifiques

- **Jean-Marc Larrue** (Université de Montréal)
- **Philippe Manevy** (Université de Montréal)

## Comité scientifique

- **Alain Chevalier** (Université de Liège, Belgique)
- **Elka Fedjuk** (Universidad Veracruzana, Mexique)
- **Kathryn Mederos Syssoyeva** (Dixie State University, USA)
- **Françoise Odin** (INSA-Lyon, France)
- **Robin Whittaker** (St. Thomas University, Canada)
- **Vito Minoia** (AENIGMA, Université d'Urbino, Italie)
