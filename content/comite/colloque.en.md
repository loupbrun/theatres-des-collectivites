---
title: Organizing Committee
description: "Members of the organizing committee for the 2020 edition of the Théâtres des collectivités colloquium."
---

- **Jean-Marc Larrue** (University of Montréal)
- **Chloée Ferland-Dufresne** (University of Montréal)
- **Dominic Poulin** (University of Montréal)
- **Yoland Roy** (FQTA)

Logistics:

- Scientific info: Philippe Manevy
- General information: Karolann St-Amand
- Lodging and transport: Camille Gascon
- Technical support: Louis-Olivier Brassard
