---
title: Home

avertissement: "**NEW DATES – Due to the current health situation, the colloquium is postponed to March 24-27, 2022.**"
---

**New dates: March 24-27, 2022**

Place: Montréal, University of Montréal

Organized by:

- The Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ)  
- The International University Theater Association (AITU-IUTA)
- The Fédération québécoise du théâtre amateur (FQTA)
- Société québécoise d’études théâtrales (SQET)

