---
title: Accueil

avertissement: "**NOUVELLES DATES – En raison de la situation sanitaire actuelle, le colloque est reporté au 24-27 mars 2022**"
---

**Nouvelles dates: 24-27 mars 2022**

Université de Montréal, Montréal, Canada

Organisé par :

- Le Centre de recherche interuniversitaire sur la littérature et la culture québécoises (CRILCQ)  
- L’Association internationale du théâtre à l’Université (AITU-IUTA)
- La Fédération québécoise du théâtre amateur (FQTA)
- Société québécoise d’études théâtrales (SQET)

